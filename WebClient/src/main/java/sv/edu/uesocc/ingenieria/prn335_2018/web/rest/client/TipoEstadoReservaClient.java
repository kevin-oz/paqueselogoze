/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.rest.client;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoReserva;

/**
 *
 * @author kevin Figueroa
 */
@Named("frmTipoEstadoReserva")
@ViewScoped
public class TipoEstadoReservaClient implements Serializable {

    TipoEstadoReserva tipoEstadoReserva;
    String select;
    String msj = null;

    Client clientesito;
    String URL = "http://localhost:8080/WebServer/rest/tipoEstadoReserva/";
    List<TipoEstadoReserva> lista;

    /**
     * constructor de la clase
     */
    public TipoEstadoReservaClient() {
        try {
            clientesito = ClientBuilder.newClient();

        } catch (Exception e) {
            System.err.println("error *** " + e.getMessage());
        }

    }

    /**
     * Todos los registros
     *
     * @return lista con todos los registros TipoEstadoReserva
     */
    public List<TipoEstadoReserva> findAll() {
        List<TipoEstadoReserva> listilla = null;
        try {
            WebTarget target = clientesito.target(URL).path("all");
            listilla = target.request(MediaType.APPLICATION_JSON).get(new GenericType<List<TipoEstadoReserva>>() {
            });

        } catch (Exception e) {

        }
        if (listilla == null) {
            listilla = Collections.EMPTY_LIST;
        }
        return listilla;
    }

    /**
     * Buscar registros por nombre
     *
     * @return lista con coincidencias de las primeras letras o el nombre
     * completo de la busqueda
     */
    public List<TipoEstadoReserva> buscarLike() {
        msj = "";
        lista = null;
        try {
            if (select != null || !select.isEmpty()) {
                WebTarget target = clientesito.target(URL).path("search").queryParam("name", select);
                lista = target.request(MediaType.APPLICATION_JSON).get(
                        new GenericType<List<TipoEstadoReserva>>() {
                });

                if (select.isEmpty() || select == null) {
                    msj = "";

                } else {
                    msj = coincidencias(lista);

                }

            }

        } catch (Exception e) {
        }
        return lista;
    }

    public String coincidencias(List list) {
        if (list.isEmpty() || list == null) {
            return "No se encontraron coincidencias";
        }
        return list.size() + " coincidencias";
    }

    /**
     * metodo inicializador
     */
    @PostConstruct
    public void init() {
        if (findAll() != null) {
            lista = findAll();
        } else {
            System.err.println("algo anda mal.jpg");
        }

    }

//*********** getters y Setters **************
    public String getMsj() {
        return msj;
    }

    public void setMsj(String msj) {
        this.msj = msj;
    }

    public TipoEstadoReserva getTipoEstadoReserva() {
        return tipoEstadoReserva;
    }

    public void setTipoEstadoReserva(TipoEstadoReserva tipoEstadoReserva) {
        this.tipoEstadoReserva = tipoEstadoReserva;
    }

    public List<TipoEstadoReserva> getLista() {
        return lista;
    }

    public void setLista(List<TipoEstadoReserva> lista) {
        this.lista = lista;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

}
